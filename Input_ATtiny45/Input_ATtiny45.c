/*
 * Input_ATtiny44A.c
 * Atmel Studio 6.1
 * Created: 6/16/2015 23:13:22
 * Author: Brandon Ruiz Vasquez
 */ 
#include <avr/io.h>     //Libreria de IO
#define F_CPU 20000000  //Cristal o Resonador
#include <util/delay.h> //Libreria del Delay
int a=0;				//Variable de Prueva
int main(void)
{
	DDRA |= (1<<DDA0) | (1<<DDA1) | (1<<DDA2) | (1<<DDA3) | (1<<DDA4) | (1<<DDA5) | (1<<DDA6) | (0<<DDA7);
	DDRB |= (1<<DDB0) | (1<<DDB1) | (1<<DDB2) | (1<<DDB3);
	/*
		DDxn PORTxn PUD (in MCUCR) I/O   Pull-up  Comment
		 0     0         X        Input    No     Tri-state (Hi-Z)
		 0     1         0        Input   Yes     Pxn will source current if ext. Pulled low
		 0     1         1        Input    No     Tri-state (Hi-Z)
		 1     0         X        Output   No     Output Low (Sink)
		 1     1         X        Output   No     Output High (Source)
	*/
    while(1)
    {
		a++;
		//La variable de prueva se usa para ver el movimiento de las instrucciones cuando usas el DEBUG practica recomendada para Personas que van empezando
		if (PINA & (1 <<PINA7)) //Queremos leer el PINA7 donde esta el PushButton
		{
			PINB |=(1 << PINB3)|(1 << PINB2)|(1 << PINB1)|(1 << PINB0);//Cambia el estado del puerto B
		/*
			Bit            7     6     5     4     3     2     1     0
			0x19 (0x39)  PINA7 PINA6 PINA5 PINA4 PINA3 PINA2 PINA1 PINA0   PINA
			Read/Write    R/W   R/W   R/W   R/W   R/W   R/W   R/W   R/W
			Initial Value N/A   N/A   N/A   N/A   N/A   N/A   N/A   N/A
		*/
			//_delay_ms(500);
		}
		else
		{
			a++;
			PORTB=0;//Apaga el puerto
		}
    }
}